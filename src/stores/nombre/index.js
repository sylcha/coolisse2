import { defineStore } from "pinia";

export const useNombreStore = defineStore("nombre", {
  state: () => ({
    nombreCourant: "1234,56789",
    nombreUtilisations: 0,
    deplacement: 0,
    zeros: "Sans",
  }),
  getters: {
    nbCourant: (state) => state.nombreCourant,
    nbUtilisations: (state) => state.nombreUtilisations,
    zerosInutiles: (state) => state.zeros,
  },
  actions: {
    augmenterNombreUtilisations() {
      this.nombreUtilisations++;
    },
    changerNombreCourant(valeur) {
      this.nombreCourant = valeur;
    },
    majDeplacement(valeur) {
      this.deplacement += valeur;
    },
    changerZerosInutiles(valeur) {
      this.zeros = valeur;
    },
  },
});
