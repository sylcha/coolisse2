import { defineStore } from "pinia";

export const useApplicationsStore = defineStore({
  id: "applications",
  state: () => ({
    nom: "",
    menuOuvert: false,
    tailleLettres: 160,
  }),
  getters: {
    appCourante: (state) => state.nom,
    menuAppEstOuvert: (state) => state.menuOuvert,
    taillePolice: (state) => state.tailleLettres,
  },
  actions: {
    basculerMenu() {
      this.menuOuvert = !this.menuOuvert;
    },
    changerApp(value) {
      this.nom = value;
    },
    changerTaillePolice(value) {
      this.tailleLettres = value;
    },
  },
});
