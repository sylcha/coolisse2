import { defineStore } from "pinia";

export const useOperationsStore = defineStore("operations", {
  state: () => ({
    nombreCourant: "12,34",
    operationEstMultiplication: true,
    demandeResultat: false,
  }),

  getters: {
    nbCourant: (state) => state.nombreCourant,
    opEstMultiplication: (state) => state.operationEstMultiplication,
    resultatDemande: (state) => state.demandeResultat,
  },

  actions: {
    changerNombreCourant(valeur) {
      this.nombreCourant = valeur;
    },
    basculerOperationCourante() {
      this.operationEstMultiplication = !this.operationEstMultiplication;
    },
    ajouterChiffre(valeur) {
      this.nombreCourant += valeur;
    },
    enleverDernierChiffre() {
      this.nombreCourant = this.nombreCourant.slice(0, -1);
    },
    razNombre() {
      this.nombreCourant = "";
    },
    basculerDemandeResultat() {
      this.demandeResultat = !this.demandeResultat;
    },
  },
});
