const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      {
        path: "nombres",
        component: () => import("src/pages/NombresPage.vue"),
      },
      {
        path: "multiplications",
        component: () => import("src/pages/MultiplicationsPage.vue"),
      },
      {
        path: "unites",
        component: () => import("src/pages/EnConstructionPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
