class Observateur {
  constructor() {
    this.observateurs = [];
  }

  souscrire(client) {
    this.observateurs.push(client);
  }

  desinscrire(client) {
    this.observateurs = this.observateurs.filter(
      (souscripteur) => souscripteur !== client
    );
  }

  avertir(value) {
    console.log("déplacement fini");
    for (let observateur of this.observateurs) {
      observateur.update(value);
    }
  }
}

export { Observateur };
