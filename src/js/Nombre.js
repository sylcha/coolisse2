/**
 * Cette classe décrit un nombre afin d'être affiché par `p5.js`
 * chiffre par chiffre. Chaque nombre admet trois propriétés :
 * (1) sa valeur initiale
 * (2) un tableau de tous ses chiffres
 * (3) le rang de l'unité dans ce tableau
 */
class Nombre {
  /**
   *
   * @param {string} value le nombre sous forme de chaîne de caractère
   */
  constructor(valeur = "12,3456") {
    this.valeurInitiale = valeur;
    const { table, rangUnite, longueurInitiale } = init(valeur);
    this.table = table;
    this.rangUnite = rangUnite;
    this.positionEnCours = rangUnite;
    this.longueurInitiale = longueurInitiale;
  }

  get valeur() {
    // la valeur retournée est un tableau
    return this.table;
  }
  set valeur(n) {
    // la valeur passée est un nombre ou une chaîne de caractères
    let chaine = String(n).replace(".", ",");
    this.valeurInitiale = chaine;
    const { table, rangUnite, longueurInitiale } = init(chaine);
    this.table = table;
    this.rangUnite = rangUnite;
    this.positionEnCours = rangUnite;
    this.longueurInitiale = longueurInitiale;
  }
  /**
   * Multiplie le nombre par 0,1
   */
  diminuer() {
    this.rangUnite -= 1;
    if (this.rangUnite < 0) {
      this.table.unshift(0);
    }
    if (this.rangUnite >= this.longueurInitiale - 1) {
      this.table.pop();
    }
  }

  /**
   * Multiplie le nombre par 10
   */
  augmenter() {
    this.rangUnite += 1;
    if (this.rangUnite <= 0) {
      this.table.shift();
    }
    if (this.rangUnite >= this.longueurInitiale) {
      this.table.push(0);
    }
  }

  /**
   * Transforme le nombre courant (stocke sous la forme d'un tableau et du rang unité)
   * en chaîne de caractères
   * @returns Chaîne de caractère correspondant au nombre
   */
  toString() {
    if (this.estEntier()) {
      return this.table.join("");
    } else {
      let t = Array.from(this.table);
      t.splice(this.rangUnite + 1, 0, ",");
      return t.join("");
    }
  }

  /**
   * Teste si le nombre courant est un entier
   * @returns `true` si le nombre courant est un entier, `false` sinon
   */
  estEntier() {
    return this.rangUnite === this.table.length - 1;
  }

  /**
   * Teste si le nombre courant en plus petit que l'unité
   * @returns `true` si le nombre courant est plus petit que l'unité, `false` sinon
   */
  estMoinsQueUn() {
    return this.rangUnite < 0;
  }
}

/**
 * Cette fonction sert fabriquer un tableau à partir de tous les chiffres d'un nombre
 * et à trouver le rang de l'unité du le nombre n passé en paramètre
 * dans ce tableau
 * @param {number} n Le nombre qu'on doit initialiser
 * @returns Un objet composé de :
 *            (1) un tableau composé de tous les chiffres du nombres
 *            (2) le rang du chiffre unité dans le tableau
 */
function init(n) {
  let rangUnite = n.indexOf(",") - 1 >= 0 ? n.indexOf(",") - 1 : n.length - 1;
  let table = n.replace(",", "").split("").map(Number);
  let longueurInitiale =
    nbZeroEnTete(table) > 0 ? table.length - 1 : table.length;
  return { table, rangUnite, longueurInitiale };
}

/**
 * Détermine le nombre de zéros figurant parmi les premiers éléments du tableau de chiffres
 * @param {Array} a Tableau de chiffres
 * @returns Nombre de zéro en tête du tableau
 */
function nbZeroEnTete(a) {
  let count = 0;
  let copy = a.slice(0);
  while (copy.shift() === 0) {
    count++;
  }
  return count;
}

export { Nombre };
