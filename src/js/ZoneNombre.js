import { Nombre } from "./Nombre.js";

export const sketch = (s) => {
  const RANGS = [
    "millions",
    "centaines de mille",
    "dizaines de mille",
    "milliers",
    "centaines", //
    "dizaines",
    "unités",
    "dixièmes",
    "centièmes",
    "millièmes",
    "dix-millièmes", //
    "cent-millièmes",
    "millionièmes",
  ];
  const DELTA = 0.02;
  const canvasHeight = 400;
  let container, fontFixed, fontRegular, largeurLettre;
  s.tailleFonte = 160;
  s.couleurFond = [25, 118, 210];
  s.nb = new Nombre();
  s.preload = () => {
    fontFixed = s.loadFont("/fonts/manti_fixed.otf");
    fontRegular = s.loadFont("/fonts/manti_regular.otf");
  };
  s.setup = () => {
    container = document.getElementById("canvasPlace");
    s.createCanvas(container.clientWidth, canvasHeight);
    // specification pour les formes
    s.rectMode(s.CENTER);
    // specification des angles
    // all angles in degrees (0 .. 360)
    s.angleMode(s.DEGREES);
    // s.nb.valeur = "";
    // s.noLoop();
  };
  s.draw = () => {
    let horizontalCenter = container.clientWidth / 2;
    let verticalCenter = canvasHeight / 2 + 75;
    const entretoise = 0.1 * s.tailleFonte;

    s.textFont(fontFixed);
    s.textSize(s.tailleFonte);
    largeurLettre = s.textWidth("2") * 0.8;
    s.espaceEntreLettres = largeurLettre + entretoise;
    s.background(s.couleurFond);
    s.fill(0);

    //decalage ?
    if (s.nb.positionEnCours < s.nb.rangUnite) {
      s.nb.positionEnCours += DELTA;
    }
    if (s.nb.positionEnCours > s.nb.rangUnite) {
      s.nb.positionEnCours -= DELTA;
    }

    // chiffres
    s.textAlign(s.CENTER, s.CENTER);
    s.fill(250);
    let decalage = s.nb.estMoinsQueUn() ? s.nb.rangUnite : 0;
    s.nb.table.forEach((chiffre, index) => {
      s.text(
        chiffre,
        horizontalCenter +
          (index + decalage - s.nb.positionEnCours) *
            (largeurLettre + entretoise),
        verticalCenter
      );
    });
    // rectangles (pour illusion case)
    s.fill(s.couleurFond);
    s.noStroke();
    for (let i = -6; i < 6; i++) {
      s.rect(
        horizontalCenter +
          entretoise / 2 +
          largeurLettre / 2 +
          i * (largeurLettre + entretoise),
        verticalCenter + 10,
        entretoise,
        s.textAscent()
      );
    }
    // rangs
    s.textAlign(s.LEFT, s.CENTER);
    s.textFont(fontRegular);
    s.textSize(s.tailleFonte * 0.11);
    s.fill(250);
    RANGS.forEach((rang, index) => {
      s.push();
      s.translate(
        horizontalCenter + (index - 6) * (largeurLettre + entretoise),
        verticalCenter - s.textAscent() * 5
      );
      s.rotate(-60);
      s.text(rang, 0, 0);
      s.pop();
    });
    // virgule
    s.textAlign(s.CENTER, s.CENTER);
    s.textSize(s.tailleFonte);
    s.fill(250);
    if (!s.nb.estEntier()) {
      s.text(",", horizontalCenter + largeurLettre * 0.6, verticalCenter);
    }
  };
  s.windowResized = () => {
    s.resizeCanvas(100, canvasHeight);
    s.resizeCanvas(container.clientWidth, canvasHeight);
  };
};
