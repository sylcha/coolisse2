/**
 * Cette classe crée un lien élastique (modèle du ressort) entre deux objets Chiffres
 */
class Lien {
  /**
   *
   * @param {number} k Coefficient d'élasticité du ressort (k<1)
   * @param {number} intervalle Distance entre les objets en équilibre (longueur du ressort au repos)
   * @param {Chiffre} a Premier chiffre à une extrémité du ressort
   * @param {Chiffre} b Deuxième chiffre à l'autre extrémité du ressort
   */
  constructor(k, intervalle, a, b) {
    this.k = k;
    this.intervalle = intervalle;
    this.a = a;
    this.b = b;
  }

  /**
   * Met à jour les tensions entre les deux objets (basées sur leur distance réciproque)
   */
  majTension() {
    let force = this.b.x - this.a.x;
    let deplacement = Math.abs(force) - this.intervalle;
    force = force / Math.abs(force);
    force *= this.k * deplacement;
    this.a.appliquerForce(force);
    force *= -1;
    this.b.appliquerForce(force);
  }
}

export { Lien };
