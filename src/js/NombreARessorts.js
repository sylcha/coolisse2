import { Chiffre } from "./Chiffre.js";
import { Lien } from "./Lien.js";

class NombreARessorts {
  /**
   * Nombre dont les chiffres sont reliés par des liens élastiques.
   * Le chiffre des unités sert de référence à l'affichage et les autres chiffres
   * s'organisent autour, notamment pour les déplacements.
   * Le lien élastique entre les chiffres doit donner l'impression que,
   * lors des déplacements, le chiffre des unités pousse et tire les autres chiffres.
   * @param {p5} instanceP5 Instance de l'objet p5
   * @param {number} nombre Nombre à utiliser
   * @param {number} intervalle Écart entre les chiffres
   * @param {number} altitude Hauteur (ordonnée) de l'affichage du nombre à l'écran
   * @param {number} raideur Raideur du lien(ressort) entre les nombres
   * @param {number} xOrigine Abscisse absolue à partir de laquelle est affichée la série
   */
  constructor(
    instanceP5,
    nombre,
    intervalle,
    altitude = 100,
    raideur = 0.07,
    xOrigine = 0
  ) {
    this.instanceP5 = instanceP5;
    this.xOrigine = xOrigine;
    this.raideur = raideur;
    this.y = altitude;
    this.intervalle = intervalle;
    let chaine = String(nombre).replace(".", ",");
    this.valeurInitiale = chaine;
    const { table, rangUnite, longueurInitiale } = init(chaine);
    this.rangUnite = rangUnite; // index du chiffre des unités dans le tableau de nombre
    // pour toujours savoir quel index dans tableau repère le chiffre devant la virgule
    this.indexChiffreDevantVirgule = rangUnite;
    // pour traquer la position du chiffre des unités à l'écran
    this.positionChiffreUnite = 0;
    this.longueurInitiale = longueurInitiale;
    this.chiffres = [];
    this.liens = [];
    this.actions = []; // garder la trace des déplacements.
    this.actionEnCours = undefined;
    this.finDeplacement = true;
    this.compteZerosAjoutesEnFin = 0; // décompte des zéros ajoutés en fin de la chaîne de départ
    this.compteZerosAjoutesEnDebut = 0; // décompte des zéros ajoutés en début de la chaîne de départ
    this.bilan = 0; // bilan des augmentations/diminutions pour le reset
    // construction de la liste des chiffres
    for (const [index, chiffre] of table.entries()) {
      let unite = index === rangUnite;
      this.chiffres.push(
        new Chiffre(
          instanceP5,
          (index - this.rangUnite) * this.intervalle,
          this.y,
          chiffre,
          unite
        )
      );
    }
    // identification du chiffre des unités dans le tableau
    this.chiffreUnite = this.chiffres[rangUnite];
    this.chiffreUnite.finDeplacement.souscrire(this);
    // construction de la liste des liens entre les chiffres
    for (let i = 0; i < this.chiffres.length - 1; i++) {
      this.liens.push(
        new Lien(
          this.raideur,
          this.intervalle,
          this.chiffres[i],
          this.chiffres[i + 1]
        )
      );
    }
  }

  /**
   * Pour avoir l'origine absolue
   * (abscisse à partir de laquelle est affichée la série sur l'écran)
   */
  get abscisse() {
    return this.xOrigine;
  }

  /**
   * Changer l'origine absolue de la position de la série sur l'écran
   * (c'est-à-dire l'abscisse à partir de laquelle est affichée la série)
   * @param {number} valeur
   */
  set abscisse(valeur) {
    this.xOrigine = valeur;
    console.log(this.xOrigine);
  }

  /**
   * Pour avoir l'ordonnée (hauteur) à laquelle est affichée la série
   */
  get ordonnee() {
    return this.y;
  }

  /**
   * Changer l'ordonnée absolue de la position de la série sur l'écran
   * (hauteur à laquelle la série est affichée sur l'écran)
   * @param {number} valeur
   */
  set ordonnee(valeur) {
    this.y = valeur;
  }

  /**
   * Mettre à jour tous les chiffres et les liens de la série
   */
  maj() {
    for (const chiffre of this.chiffres) {
      chiffre.majPosition();
    }
    for (const lien of this.liens) {
      lien.majTension();
    }
    if (!this.actionEnCours) {
      // pas d'action en cours
      if (this.actions.length !== 0) {
        // il reste des actions à faire, on prend la première de la pile
        this.actionEnCours = this.actions.shift();
        this.finDeplacement = false;
        // on met à jour la position du chiffre unité
        switch (this.actionEnCours.action) {
          case "augmentation":
            this.chiffreUnite.changerDestination(
              this.chiffreUnite.x - this.intervalle
            );
            this.positionChiffreUnite -= 1;
            this.indexChiffreDevantVirgule += 1;
            break;
          case "diminution":
            this.chiffreUnite.changerDestination(
              this.chiffreUnite.x + this.intervalle
            );
            this.positionChiffreUnite += 1;
            this.indexChiffreDevantVirgule -= 1;
            break;
        }
      }
    } else {
      // il y a une action en cour, on effectue les déplacements

      // le déplacement est-il fini ?
      if (this.finDeplacement) {
        // mise à jour des chiffres du nombre (ajout de zéro, etc.)
        switch (this.actionEnCours.action) {
          case "augmentation":
            // si le dernier chiffre est en position "unitaire" on ajoute un zéro
            if (this.rangUniteEnCours() >= this.chiffres.length) {
              const positionDernierChiffre =
                this.chiffres[this.chiffres.length - 1].x;
              this.chiffres.push(
                new Chiffre(
                  this.instanceP5,
                  positionDernierChiffre + this.intervalle,
                  this.y,
                  "0",
                  false
                )
              );
              this.liens.push(
                new Lien(
                  this.raideur,
                  this.intervalle,
                  this.chiffres[this.chiffres.length - 2],
                  this.chiffres[this.chiffres.length - 1]
                )
              );
              this.compteZerosAjoutesEnFin += 1;
            }
            // enlever les zéros en tête si on a ajouté précédemment
            if (this.compteZerosAjoutesEnDebut > 0) {
              this.chiffres.shift();
              this.liens.shift();
              this.compteZerosAjoutesEnDebut -= 1;
              this.rangUnite -= 1;
            }
            break;
          case "diminution":
            // enlever le dernier chiffre s'il est après la virgule et si c'est zéro
            if (this.compteZerosAjoutesEnFin > 0) {
              this.chiffres.pop();
              this.liens.pop();
              this.compteZerosAjoutesEnFin -= 1;
            }
            // ajouter un zéro
            if (this.indexChiffreDevantVirgule < 0) {
              const premierChiffre = this.chiffres[0];
              this.chiffres.unshift(
                new Chiffre(
                  this.instanceP5,
                  premierChiffre.x - this.intervalle,
                  this.y,
                  "0",
                  false
                )
              );
              this.liens.unshift(
                new Lien(
                  this.raideur,
                  this.intervalle,
                  this.chiffres[0],
                  this.chiffres[1]
                )
              );
              this.rangUnite += 1;
              this.compteZerosAjoutesEnDebut += 1;
            }
            break;
        }
        // plus d'action en cours
        this.actionEnCours = undefined;
      }
    }
  }

  /**
   * Afficher la série de chiffres
   */
  afficher() {
    for (const chiffre of this.chiffres) {
      chiffre.afficher(undefined, this.y, this.xOrigine);
    }
  }

  /**
   * Changer l'intervalle entre les chiffres
   * @param {number} nouvelEcart Intervalle à mettre à jour
   */
  changerIntervalle(nouvelEcart) {
    this.intervalle = nouvelEcart;
    for (let [index, chiffre] of this.chiffres.entries()) {
      chiffre.changerAbscisse((index - this.rangUnite) * nouvelEcart);
      chiffre.changerDestination(chiffre.x);
    }
    for (let lien of this.liens) {
      lien.intervalle = nouvelEcart;
    }
  }

  /**
   * Multiplier le nombre par 10
   */
  augmenter() {
    this.bilan += 1;
    this.actions.push({
      action: "augmentation",
      positionCUAvantDeplacement: this.chiffreUnite.x,
    });
  }

  /**
   * Multiplier le nombre par 0,1
   */
  diminuer() {
    this.bilan -= 1;
    this.actions.push({
      action: "diminution",
      positionCUAvantDeplacement: this.chiffreUnite.x,
    });
  }

  /**
   * Repositionner le nombre dans sa position de départ
   * (le chiffre des unités de départ revient devant la virgule)
   */
  reset() {
    if (this.bilan > 0) {
      for (let i = this.bilan; i > 0; i--) {
        this.diminuer();
      }
    }
    if (this.bilan < 0) {
      for (let i = -1 * this.bilan; i > 0; i--) {
        this.augmenter();
      }
    }
  }

  aBesoinDeVirgule() {
    let d = this.rangUnite - this.positionChiffreUnite;
    return d < this.chiffres.length - 1;
  }

  rangUniteEnCours() {
    return this.rangUnite - this.positionChiffreUnite;
  }

  update(chiffre) {
    this.finDeplacement = true;
  }
}

/**
 * Cette fonction sert fabriquer un tableau à partir de tous les chiffres d'un nombre
 * et à trouver le rang de l'unité du le nombre n passé en paramètre
 * dans ce tableau
 * @param {String} n Le nombre qu'on doit initialiser
 * @returns Un objet composé de :
 *            (1) un tableau composé de tous les chiffres du nombres
 *            (2) le rang du chiffre unité dans le tableau
 */
function init(n) {
  let rangUnite = n.indexOf(",") - 1 >= 0 ? n.indexOf(",") - 1 : n.length - 1;
  let table = n.replace(",", "").split("").map(Number);
  let longueurInitiale =
    nbZeroEnTete(table) > 0 ? table.length - 1 : table.length;
  return { table, rangUnite, longueurInitiale };
}

/**
 * Détermine le nombre de zéros figurant parmi les premiers éléments du tableau de chiffres
 * @param {Array} a Tableau de chiffres
 * @returns Nombre de zéro en tête du tableau
 */
function nbZeroEnTete(a) {
  let count = 0;
  let copy = a.slice(0);
  while (copy.shift() === 0) {
    count++;
  }
  return count;
}

export { NombreARessorts };
