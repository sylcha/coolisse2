import { p5 } from "p5";
import { Observateur } from "./Observateur.js";
const DELTA = 5; // pas de déplacement du chiffre (pour régler la vitesse de déplacement)
class Chiffre {
  /**
   * Cette classe définit un chiffre pouvant se déplacer.
   * @param {p5} p Instance de l'objet p5
   * @param {number} x abscisse absolue de la position du chiffre dans la zone
   * @param {number} y ordonnée absolue de la position du chiffre dans la zone
   * @param {string} signe caractère à afficher ("0" à "9" en principe)
   * @param {boolean} estUnite le chiffre courant est-il le chiffre des unités ?
   * @param {number} destination position (abscisse) à laquelle doit se rendre le chiffre
   * @param {array} couleur description de la couleur
   * (tableau : [R, G, B, alpha] avec valeurs entre )
   */
  constructor(
    p = p5.instance,
    x = 0,
    y = 0,
    signe = "0",
    estUnite,
    destination = 0,
    couleur = [17, 45, 78]
  ) {
    this.p = p;
    this.signe = signe;
    this.x = x;
    this.y = y;
    this.vitesse = 0;
    this.estUnite = estUnite;
    this.destination = destination;
    // si le chiffre est celui des unités, on lui attribue une couleur spéciale pour le différencier
    if (!this.estUnite) {
      this.couleur = couleur;
    } else {
      this.couleur = [17, 45, 78, 150];
    }
    this.finDeplacement = new Observateur();
  }

  /**
   * Changer la couleur d'affichage du chiffre
   * @param {number || Array } autreCouleur Couleur à appliquer [R,B,G,alpha]
   */
  changerCouleur(autreCouleur) {
    this.couleur = autreCouleur;
  }

  /**
   *
   * @param {number} force le vecteur force agissant sur le chiffre
   */
  appliquerForce(force) {
    this.vitesse += force;
  }

  /**
   * Mise à jour de la position
   */
  majPosition() {
    if (this.estUnite) {
      // l'unité sert d'ancre pour les autres chiffres :
      // elle a son propre déplacement indépendant
      if (this.x !== this.destination) {
        // on est pas encore arrivé !
        if (Math.abs(this.x - this.destination) > DELTA) {
          // destination en cours ?
          let v = this.destination - this.x;
          v = v / Math.abs(v);
          v *= DELTA;
          this.x += v;
          // console.log("x : " + this.x + " / destination : " + this.destination);
        } else {
          // à la fin
          this.x = this.destination;
          this.finDeplacement.avertir(this);
        }
      }
    } else {
      this.x += this.vitesse;
      this.vitesse *= 0.8;
    }
  }

  /**
   * Afficher le chiffre à sa position
   */
  afficher(abscisse = this.x, ordonnee = this.y, base = 0, intervalle = 1) {
    this.p.fill(this.couleur);
    this.p.text(this.signe, abscisse + base, ordonnee);
  }

  /**
   * Changer l'abscisse du chiffre
   * @param {number} nouvelX Nouvelle abscisse
   */
  changerAbscisse(nouvelX) {
    this.x = nouvelX;
  }

  /**
   * Changer la destination du chiffre
   * @param {number} nouvelleDestination Abscisse à transférer
   */
  changerDestination(nouvelleDestination) {
    this.destination = nouvelleDestination;
  }
}

export { Chiffre };
